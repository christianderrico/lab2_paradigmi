package u02

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class FourthExercise {

  def compose(f: Int => Int, g: Int => Int): Int => Int = i => f(g(i))

  @Test def testCompose(): Unit ={

    val result = compose(_-1, _*2)(5)
    assertEquals(result, 9)
  }

  def genericCompose[T](f: T => T, g: T => T): T => T = t => f(g(t))

  @Test def testGenericCompose() = {

    val result = genericCompose[String]("" + _ + ", how are u?", "Hello " + _)("Christian")
    assertEquals(result, "Hello Christian, how are u?")
  }
}
