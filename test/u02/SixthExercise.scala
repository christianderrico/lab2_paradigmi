package u02

import java.lang

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.{Assertions, Test}

sealed trait Shape

case class Circle(radius: Int) extends Shape
case class Square(side: Int) extends Shape
case class Rectangle(longSide: Int, shortSide: Int) extends Shape

object geomOps {

  private def roundingValue(x: Double): Double = Math.round(x * 100d) / 100d
  private val pi = roundingValue(Math.PI)

  def perimeter(shape: Shape): Double = shape match {
    case Circle(radius) => roundingValue(2 * radius * pi)
    case Rectangle(longSide, shortSide) => (longSide + shortSide) * 2
    case Square(side) => side * 4
  }

}

class SixthExercise {

  import geomOps._
  @Test def testPerimeter(): Unit ={
    val radius = 5
    val circumference = 31.4
    assertEquals(perimeter(Circle(radius)), circumference)

    val side = 4
    val squarePerimeter = 16
    assertEquals(perimeter(Square(side)), squarePerimeter)

    val shortSide = 2
    val longSide = 5
    val rectanglePerimeter = 14
    assertEquals(perimeter(Rectangle(longSide, shortSide)), rectanglePerimeter)
  }

}
