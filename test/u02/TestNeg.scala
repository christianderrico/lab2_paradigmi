package u02

import org.junit.jupiter.api.Assertions.{assertFalse, assertTrue}
import org.junit.jupiter.api.Test

class TestNeg {

  def neg: (String => Boolean) => (String => Boolean) = f => (x => !f(x))

  @Test def testNeg(){

    val empty: String => Boolean = _==""
    val notEmpty = neg(empty)
    assertTrue(empty(""))
    assertFalse(notEmpty(""))
    assertTrue(notEmpty("hello"))

  }

  def genericNeg[A]: (A => Boolean) => (A => Boolean) = f => (obj => !f(obj))

  @Test def testGericNeg(): Unit ={

    val isEven: Int => Boolean = _ % 2 == 0
    val isOdd = genericNeg(isEven)

    assertTrue(isEven(2))
    assertFalse(isOdd(2))
    assertTrue(isOdd(5))
  }

}
