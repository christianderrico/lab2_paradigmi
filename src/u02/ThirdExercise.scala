package u02

object ThirdExercise extends App {

  val p1: Int => Int => Int => Boolean = x => y => z => x <= y && y <= z
  println("p1 => (expected: true), (actual: " + p1(3)(4)(5) + ")")
  println("p1 => (expected: false), (actual: " + p1(4)(5)(2) + ")")

  val p2: (Int, Int, Int) => Boolean = (x, y, z) => x <= y && y <= z
  println("p2 => (expected: true), (actual: " + p2(3, 4, 5) + ")")
  println("p2 => (expected: false), (actual: " + p2(4, 5, 2) + ")")

  def p3(x: Int)(y: Int)(z: Int): Boolean = x <= y && y <= z
  println("p3 => (expected: true), (actual: " + p3(3)(4)(5) + ")")
  println("p3 => (expected: false), (actual: " + p3(4)(5)(2) + ")")

  def p4(x: Int, y: Int, z: Int): Boolean = x <= y && y <= z
  println("p4 => (expected: true), (actual: " + p4(3, 4, 5) + ")")
  println("p4 => (expected: false), (actual: " + p4(4, 5, 2) + ")")
}
