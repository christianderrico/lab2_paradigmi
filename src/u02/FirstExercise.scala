package u02

object FirstExercise extends App {

  val parity: Int => String = x => if (x % 2 == 0) "even" else "odd"

  def parity2(x: Int): String = {
    if (x % 2 == 0) "even" else "odd"
  }

  val parity3: Int => String = {
    case x if x % 2 == 0 => "even"
    case x => "odd"
  }

  println(parity(2)) //even
  println(parity(1)) //odd

  println(parity2(2)) //even
  println(parity2(1)) //odd

  println(parity3(2)) //even
  println(parity3(1)) //odd

}
